<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{

    private $categoryArray = [
        'Ethiopia',
        'Meat',
        'Beef',
        'Chili pepper',
        'China', 
        'Fish', 
        'Tofu', 
        'Sichuan pepper',
        'Peru', 
        'Potato',
        'Yellow Chili pepper',
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->categoryArray as $category) {
        	Category::create(['name' => $category]);
        }
    }
}
