<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $jsonData = Storage::get('json/spicy-deli.json');

        $productArray = json_decode($jsonData, true);

        foreach ($productArray['products'] as $product) {
        	Product::create($product);
        }
    }
}
