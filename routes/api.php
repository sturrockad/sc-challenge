<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
 
   return $request->user();
 
});

Route::any('/login', 'UserController@login')->name('login');
Route::post('/register', 'UserController@register')->name('register');

	Route::group(['prefix' => 'products'], function () {
		Route::post('/', 'ProductController@save')->name('products.save');
		Route::get('/{id}', 'ProductController@get')->name('products.get');
		Route::put('/{id}', 'ProductController@update')->name('products.update');
		Route::delete('/{id}', 'ProductController@delete')->name('products.delete');
		Route::get('/', 'ProductController@index')->name('products.index');
	});

	Route::get('/categories/', 'CategoryController@index')->name('categories.index');
