<?php 

class ProductListCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToTest(ApiTester $I)
    {

    }

    public function createUserViaAPI(\ApiTester $I)
    {

    	$data = [
            'name' => 'Test Product',
            'sku' => '1234ABC',
            'price' => '10.00',
            'category' => 'Test',
        ];

        $I->amHttpAuthenticated('api_user', 'test');
        $I->sendPOST('/products', $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains('{"result":"ok"}');
        
    }
}
