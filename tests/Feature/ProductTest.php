<?php

namespace Tests\Feature;

use App;
use App\Models\Product;
use Tests\DatabaseTestCase;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\ProductRepository;

class ProductTest extends DatabaseTestCase
{

    use CreatesApplication;


    public function setUp()
    {
        parent::setUp();
        $this->repository = App::make(ProductRepository::class);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAllProducts()
    {


        $productList = $this->repository->getAllProducts();

        $this->assertTrue($productList->count() > 0);
        $this->assertTrue(get_class($productList[0]) == App\Models\Product::class);

    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInsertProduct()
    {
        $data = [
            'name' => 'Test Product',
            'sku' => '1234ABC',
            'price' => '10.00',
            'category' => 'Test',
        ];

        $productRecord = $this->repository->updateOrCreate(['name' => 'Test Product'], $data);

        $this->assertTrue($productRecord->name === 'Test Product');
    }

    public function testEditProduct()

    {
        $this->testInsertProduct();

        $testProduct = Product::where('name', 'Test Product')->first();

        $this->assertNotEmpty($testProduct);

        $newData = [
            'price' => '20.00',
        ];

        $newProduct = $this->repository->updateOrCreate(['name' => 'Test Product'], $newData);
        $this->assertTrue($newProduct->price == 20.00);

    }

}
