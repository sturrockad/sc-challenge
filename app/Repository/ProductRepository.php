<?php 

namespace App\Repositories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Collection;

class ProductRepository
{
	private $product;


	public function __construct(Product $product)
	{
		$this->product = $product;
	}

	public function getAllProducts() : Collection
	{
		return Product::all();
	}

	public function saveAll(array $products) {
		foreach ($products as $product) {
			$this->updateOrCreate(['name' => $product['name']], $product);
		}
	}

	public function updateOrCreate(array $attributes, array $data) {
		if (isset($data['category'])) {
		    $category = Category::firstOrCreate(['name' => $data['category']]);
	    }
		return $this->product->updateOrCreate($attributes, $data);
	}
}