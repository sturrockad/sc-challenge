<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{


	private $repository;

	public function __construct(ProductRepository $repository) {
		$this->repository = $repository;
	}

    public function index()
    {
    	$products = Product::all();
    	return response()->json($products);
    }

    public function get(int $productId)
    {
    	$product = Product::findOrFail($productId);

    	return response()->json($product);
    }

    public function delete(int $productId) {
    	$product = Product::findOrFail($productId);

    	$product->delete();

    	return responses()->json([
    		'status' => 'success',
    	    'message' => "Deleted product with id {$productId}"
    	]);

    }

    public function update(int $productId) {
    	$data = json_decode($request->getContent(), true);

    	$this->repository->updateOrCreate(['id' => $productId], $data);
    	return response()->json($product);
    }

	public function save(Request $request) {

	   $data = json_decode($request->getContent(), true);


       $validator = Validator::make($data, [
 
        'products.*.name' => 'required|string',
        'products.*.sku' => 'required',
        'products.*.price' => 'required',
        'products.*.category' => 'required|string'
 
       ]);		

       if ($validator->fails()) {
 
           return response()->json(['error'=>$validator->errors()], 401);            
 
       }
 
 

		$this->repository->saveAll($data['products']);

		return response()->json([
			'status' => 'success']);
	}

}
