<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{


	public function __construct() {
	}

    public function index()
    {
    	$categories = Category::all()->pluck('name', 'id');
    	return response()->json($categories);
    }
}
